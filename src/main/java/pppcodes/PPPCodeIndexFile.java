/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.BucketStorageException;
import messif.buckets.storage.LongStorage;
import messif.buckets.storage.impl.DiskStorage;
import mindex.MIndexProperties;
import mindex.navigation.VoronoiInternalCell;
import pppcodes.index.persistent.PPPCodeInternalCellFile;
import pppcodes.index.persistent.PPPCodeLeafCellFile.PPPCodeLeafData;

/**
 * Version of PPP-Code index configuration file that maintains the Voronoi leaf data in a separate
 *  disk storage.
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeIndexFile extends PPPCodeIndex {
    
    /** Class serial ID for serialization */
    private static final long serialVersionUID = 112401L;

    /** File name where the leaf nodes are stored (and updated every time). */
    protected DiskStorage<PPPCodeLeafData> leafStorage = null;
            
    // **************************************    Initialization   ************************************* //

    /**
     * Creates new instance of M-Index having a file name of M-Index properties.
     * @param origConfiguration created properties specifying M-Index configuration
     * @param prefix prefix of the properties (e.g. "mindex.") for this PPP-Code single index
     * @throws java.lang.InstantiationException if the file does not contain what it should contain
     * @throws messif.algorithms.AlgorithmMethodException if anything else goes wrong
     */
    public PPPCodeIndexFile(Properties origConfiguration, String prefix) throws InstantiationException, AlgorithmMethodException {
        super(origConfiguration, prefix);
    }
    
    protected PPPCodeIndexFile(MIndexProperties config) throws InstantiationException, AlgorithmMethodException {
        super(config);
    }

    //************************************   Getters ************************//

    /** 
     * Returns the dynamic M-Index tree root - it is of the type {@link PPPCodeInternalCellFile}.
     */
    @Override
    public PPPCodeInternalCellFile getVoronoiCellTree() {
        return (PPPCodeInternalCellFile) voronoiCellTree;
    }
    
    /**
     * Returns the leaf disk storage - may be null.
     * @return the leaf disk storage - may be null
     */
    public LongStorage<PPPCodeLeafData> getLeafStorage() {
        return leafStorage;
    }
    
    /**
     * If the PPP-Codes are configured using {@link PPPCodeIndexFile} then this method can
     *  set the storage to be used to store the leave node data. This method is efficient only
     *  for the first time - after that all leaves have links to a specific storage.
     * @param leafStorage disk storage to store the leaf data
     * @throws messif.buckets.BucketStorageException if write to the storage fails
     */
    public void setLeafStorage(DiskStorage<PPPCodeLeafData> leafStorage) throws IllegalStateException, BucketStorageException {
        if (this.leafStorage != null) {
            throw new IllegalStateException("the leaf storage is already set and cannot be changed");
        }
        this.leafStorage = leafStorage;
        getVoronoiCellTree().writeToStorage();
    }

    /**
     * This is just a testing method that should not be called under normal circumstances.
     * Reads all the data from PPP-Code file storage into the memory.
     * @throws BucketStorageException if the reading fails
     */
    public void reloadDataTest() throws BucketStorageException {
        getVoronoiCellTree().reloadDataTest();
    }
    
    @Override
    public void finalize() throws Throwable {
        leafStorage.finalize();
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    protected VoronoiInternalCell initVoronoiCellTree() throws AlgorithmMethodException {
        return new PPPCodeInternalCellFile(this, null, new short[0]) ;
    } 
    
    /** 
     * Prints info about this M-Index.
     * @return string representation of this M-Index
     */
    @Override
    public String toString() {
        StringBuilder strBuf = new StringBuilder(super.toString());
        strBuf.append("Leaf cells in file storage: ").append((leafStorage == null) ? "null" : leafStorage.toString()).append("\n");
        return strBuf.toString();
    }
    
}
