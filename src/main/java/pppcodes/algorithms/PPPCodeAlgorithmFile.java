/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.algorithms;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.Algorithm;
import messif.buckets.BucketStorageException;
import messif.buckets.storage.impl.DiskStorage;
import mindex.algorithms.MIndexAlgorithm;
import pppcodes.PPPCodeIndexFile;
import pppcodes.ids.LocatorStringIntConvertor;
import pppcodes.index.persistent.PPPCodeLeafCellFile;

/**
 * This is an algorithm for PPP Codes that internally uses several standard M-Index algorithms.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeAlgorithmFile extends PPPCodeAlgorithm {
    
    /** Class id for serialization. */
    private static final long serialVersionUID = 212002L;

    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     * @param algorithms the algorithms on which the operations are processed
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor with created algorithms", arguments = {"array of running algorithms"})
    public PPPCodeAlgorithmFile(Algorithm[] algorithms) {
        super(algorithms);
    }

    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     * @param algorithms the algorithms on which the operations are processed
     * @param locatorConvertor convertor used for string-integer locator conversion and back again
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor with created algorithms", arguments = {"array of running algorithms", "converter of locator strings to integer"})
    public PPPCodeAlgorithmFile(Algorithm[] algorithms, LocatorStringIntConvertor locatorConvertor) {
        super(algorithms, locatorConvertor);
    }
    
    /**
     * Checks all the M-Index algorithms, if their dynamic cell tree was not modified and
     *  serializes the whole algorithm if it was changed. THis method is typically called
     *  periodically.
     * @param serializationFile file to serialize the whole algorithm into
     * @throws IOException if the serialization failed
     */
    @Override
    public void checkModifiedAndStore(String serializationFile) throws IOException {
        serializingLock.lock();
        try {
            boolean anyModified = false;
            for (MIndexAlgorithm mIndexAlgorithm : algorithms) {
                anyModified = anyModified || mIndexAlgorithm.getmIndex().isDynamicTreeModified();
            }
            if (anyModified) {
                // Store algorithm to file
                this.storeToFile(serializationFile);
            }
        } finally {
            serializingLock.unlock();
        }
    }   
    
    /**
     * If the PPP-Codes are configured using {@link PPPCodeIndexFile} then this method can
     *  set the storage to be used to store the leave node data. This method is efficient only
     *  for the first time - after that all leaves have links to a specific storage.
     * @param leafStorage disk storage to store the leaf data of all PPP-Code overlays
     */
    public void setLeafStorage(DiskStorage<PPPCodeLeafCellFile.PPPCodeLeafData> leafStorage) throws IllegalStateException, BucketStorageException {
        consolidateTreeData();
        for (PPPCodeSingleAlgorithm singleAlg : algorithms) {
            if (singleAlg.getmIndex() instanceof PPPCodeIndexFile) {
                ((PPPCodeIndexFile) singleAlg.getmIndex()).setLeafStorage(leafStorage);
            }
        }
        Logger.getLogger(PPPCodeAlgorithmFile.class.getName()).log(Level.INFO, "leaf disk storage set to: {0}", leafStorage.getFile());
    }

}
