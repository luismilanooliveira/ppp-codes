/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pppcodes.algorithms;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.Algorithm;

/**
 *
 * @author david
 * @deprecated it was never finished nor used
 */
@Deprecated
public class ConvertFileAlgorithm {
    
    public static void main (String [] args) {
        try {
            if (args.length < 2) {
                System.err.println("Usage: " + ConvertFileAlgorithm.class + " <input-ser-file> <output-ser-file>");
                return;
            }
            PPPCodeAlgorithm restoreFromFile = Algorithm.restoreFromFile(args[0], PPPCodeAlgorithm.class);
            for (PPPCodeSingleAlgorithm algorithm : restoreFromFile.algorithms) {
                PPPCodeSingleAlgorithmFile fileAlg = (PPPCodeSingleAlgorithmFile) algorithm;
                fileAlg.getmIndex();
                //new PPPCodeSingleAlgorithm(null, null);
                        
            }
        } catch (IOException | NullPointerException | ClassNotFoundException | ClassCastException ex) {
            Logger.getLogger(ConvertFileAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
