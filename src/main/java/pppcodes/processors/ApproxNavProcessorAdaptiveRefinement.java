/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.processors;

import java.util.Collections;
import java.util.List;
import messif.objects.LocalAbstractObject;
import messif.operations.query.ApproxKNNQueryOperation;
import pppcodes.algorithms.PPPCodeSingleAlgorithm;
import pppcodes.ids.IDObjectRAStorage;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
@Deprecated
public class ApproxNavProcessorAdaptiveRefinement extends ApproxNavProcessorRefinement {
    
    /** Name of parameter to manage the check step {@link #checkStep}. */
    public final static String PARAM_CHECK_STEP = "CHECK_STEP";
    
    /** Default step (number of inserted objects) after which check the improvement. */
    public static final int DEFAULT_CHECK_STEP = 100;

    /** The minimum number of steps to proceed. */
    public static final int MIN_NR_OF_STEPS = 8;

    /** The minimum number of steps to proceed. */
    public static final int MAX_NR_OF_STEPS = 16;
    
    /** Actual stop(number of inserted objects) after which check the improvement. */
    protected final int checkStep;
    
    /**
     * Creates new navigation processor for given operation list of individual PPP-Code indexes; this
     *  navigation processor refines the candidate set and adaptively decides when to end the
     *  processing by checking the continuous improvement of the result set.
     * @param operation approximate kNN operation
     * @param idObjectStorage ID-object hash storage (random access indexed file, for instance)
     * @param indexes list of individual indexes
     */
    public ApproxNavProcessorAdaptiveRefinement(IDObjectRAStorage<? extends LocalAbstractObject> idObjectStorage, 
            ApproxKNNQueryOperation operation, List<PPPCodeSingleAlgorithm> indexes) {
        super(idObjectStorage, operation, indexes);
        int step = operation.getParameter(PARAM_CHECK_STEP, Integer.class, DEFAULT_CHECK_STEP);
        if (step <= 0) {
            step = DEFAULT_CHECK_STEP;
        }
        this.checkStep = step;
    }

    protected int lastCheckPoint = 0;
    protected float lastQueryRadius = LocalAbstractObject.MAX_DISTANCE;
    
    @Override
    protected boolean continuteDCRefinement() {
        if (candSizeProcessed.get() >= MAX_NR_OF_STEPS * checkStep) {
            return false;
        }
        if (candSizeProcessed.get() >= MIN_NR_OF_STEPS * checkStep && candSizeProcessed.get() - lastCheckPoint >= checkStep) {
            float currentRadius = originalOperation.getAnswerThreshold();
            try {
                return (lastQueryRadius == LocalAbstractObject.MAX_DISTANCE ||  currentRadius < lastQueryRadius);
            } finally {
                lastCheckPoint = candSizeProcessed.get();
                lastQueryRadius = currentRadius;
            }
        }
        return true;
    }
    
    @Override
    protected boolean continuteObjectResolving() {
        if (refinementToEnd.get() <= 0 && refinementToStart.get() <= 0 ) {
            currentlyReadObjects.clear();
            currentlyReadObjects.add(Collections.singletonList((LocalAbstractObject) null).iterator());
            return false;
        }
        return true;
    }
    
    @Override
    protected boolean continueMerging() {
        // create the statistics
        int objectsMerged = nObjectsResolved + candidateIDQueue.size();
        updateStatistics(objectsMerged);
        if (refinementToEnd.get() <= 0 && refinementToStart.get() <= 0) {
            // add a fake integer to the candidate FIFO so that the ID resolvement thread does not wait forever
            candidateIDQueue.clear();
            candidateIDQueue.add(-1);
            return false;
        }
        return true;
    }
}
