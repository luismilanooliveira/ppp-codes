/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.processors;

import gnu.trove.iterator.TIntIterator;
import java.util.Comparator;
import pppcodes.index.PPPCodeLeafCell;
import java.util.Iterator;
import java.util.NoSuchElementException;
import messif.algorithms.AlgorithmMethodException;
import messif.operations.RankingSingleQueryOperation;
import mindex.MetricIndex;
import mindex.MetricIndexes;
import mindex.processors.*;
import pppcodes.index.RankableLocators;

/**
 * Navigation processor approx kNN operations on the PPP-COde version of M-Index.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class SingleSpaceCandGenerator extends ApproxNavigationProcessor {
    
    /** Counter of data objects added to the queue */
    protected volatile int objectsAddedToQ;
    
    /**
     * Creates new approximate navigation processor for given operation and M-Index. The priority queue is initialized.
     * @param operation approximate operation - typically kNN or range (should be the same for approximate range)
     * @param mIndex M-Index object with configuration and dynamic cell tree
     * @throws AlgorithmMethodException if the M-Index cannot determine the PP for query object
     */
    public SingleSpaceCandGenerator(RankingSingleQueryOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        //super(operation, null, mIndex, new PriorityQueue<>(1000, cellObjectPPPComparator), false);
        super(operation, null, mIndex, new PartialPriorityQueue<>(1000, cellObjectPPPComparator), false);
    }  
        
    @Override
    protected RankingSingleQueryOperation processItem(RankingSingleQueryOperation operation, SinglePPPRankable processingItem) throws AlgorithmMethodException {
        RankableCell rankableCell = ((RankableCell) processingItem);
        try {
            // evaluate the operation on relevant data
            Iterator<RankableLocators> leafObjects = ((PPPCodeLeafCell) rankableCell.getCell()).getRankableLocators(queryPPPDistCalculator, rankableCell.getPPP(), rankableCell.getOwnQueryDistance());
            int objCount = 0;
            while (leafObjects.hasNext()) {
                priorityQueue.add(leafObjects.next());
                objCount ++;
            }
            objectsAddedToQ += objCount;
            return operation;
        } finally {
            rankableCell.getCell().readUnLock();            
        }
    }
    
    /**
     * Creates and returns an iterator over the PPP codes from the head of the queue
     *  that have the same distance (typically objects with the same PPP code).
     * If the "hasNext() == false" is achieved then all returned objects are removed from the underlying collection
     * @return iterator over PPP-code objects that should be processed
     */
    @SuppressWarnings("empty-statement")
    public TIntIterator getTopIDs() {
        try {
            while (processStep()); // This empty body is intended
        } catch (AlgorithmMethodException | CloneNotSupportedException | InterruptedException ex) {
            MetricIndexes.logger.severe(ex.getLocalizedMessage());
        }
        return new FirstSetIterator();
    }
    
    /**
     * Finish the thread that takes care about reading and sorting the PPP-Code data from leaf cells.
     */
    public void finishReading() {
    }
    
    // internal iterator over the first N objects with the same distance from the query object
    protected class FirstSetIterator implements TIntIterator {
        // distance to the last returned object
        float lastDistance = Float.MAX_VALUE;
        // flag if the next object can be returned
        boolean hasNext = false;
        int [] topInts = null;
        int topIntIndex = 0;
                
        @Override
        public boolean hasNext() {
            if (hasNext || (topInts != null && topIntIndex < topInts.length)) {
                return hasNext = true;
            }
            SinglePPPRankable top = priorityQueue.isEmpty() ? null : priorityQueue.peek();
            return hasNext = top != null && (top.getQueryDistance() <= lastDistance) && (top instanceof RankableLocators);
        }

        @Override
        public int next() {
            if (! hasNext) {
                throw new NoSuchElementException("all objects with the same distance were returned (or hasNext() not called)");
            }
            try {
                if (topInts == null || topIntIndex >= topInts.length) {
                    RankableLocators top = (RankableLocators) priorityQueue.poll();
                    queueItemsProcessed ++;
                    topInts = top.getLocators();
                    topIntIndex = 0;
                    lastDistance = top.getQueryDistance();
                }
                return topInts[topIntIndex];
            } finally {
                hasNext = false;
                topIntIndex ++;
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("This is a read only iterator");
        }
    }

    /**
     * Comparator between objects or M-Index cells according to query-PPP distance.
     */
    public static Comparator<SinglePPPRankable> cellObjectPPPComparator = new Comparator<SinglePPPRankable>() {

        @Override
        public int compare(SinglePPPRankable o1, SinglePPPRankable o2) {
            int compare = Float.compare(o1.getQueryDistance(), o2.getQueryDistance());
            if (compare == 0) {
                if (o2 instanceof RankableCell && o1 instanceof RankableLocators) {
                    return 1;
                }
                if (o1 instanceof RankableCell && o2 instanceof RankableLocators) {
                    return -1;
                }
            }
            return compare;
        }
    };
}

