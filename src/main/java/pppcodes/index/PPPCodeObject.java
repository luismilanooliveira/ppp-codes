/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.index;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.util.Arrays;
import messif.objects.LocalAbstractObject;
import messif.objects.keys.AbstractObjectKey;

/**
 * This object represents a single pivot permutation prefix of an object according to
 *  some set of pivots.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeObject extends LocalAbstractObject {

    /** Serialization UID. */
    private static final long serialVersionUID = 961002L;
    
    /** Single PPP Code (pivot permutation prefix) */
    protected short [] ppp;
    
    /** ID(s) corresponding to given PPP */
    private final int [] locators;
    
    
    // ***************** Constructors *****************//
    
    /**
     * Creates new PPPCode object from given integer locator and PPP. THe past PPP is not cloned.
     * @param locator as integer
     * @param ppp pivot permutation prefix
     */
    public PPPCodeObject(int locator, final short [] ppp) {
        this.locators = new int [] {locator};
        this.ppp = ppp;
    }

    /**
     * Creates new PPPCode object from given integer locator and PPP. THe past PPP is not cloned.
     * @param originalKey original key (locator) to be passed to {@link LocalAbstractObject}
     * @param locator as integer
     * @param ppp pivot permutation prefix
     */
    public PPPCodeObject(AbstractObjectKey originalKey, int locator, final short [] ppp) {
        super(originalKey);
        this.locators = new int [] {locator};
        this.ppp = ppp;
    }
    
    /**
     * Creates a single PPPCode object given a list of lists of IDs to be stored in this new object.
     * @param locatorDoubleArray list of lists of IDs to be stored in this new object
     * @param overallLocatorLength overall length of the passed ID arrays
     * @param ppp PPP-Code of the newly created object
     */
    public PPPCodeObject(int [] [] locatorDoubleArray, int overallLocatorLength, final short [] ppp) {        
        this.ppp = ppp;
        this.locators = new int[overallLocatorLength];
        int j = 0;
        for (int[] subArray : locatorDoubleArray) {
           System.arraycopy(subArray, 0, this.locators, j, subArray.length);
           j += subArray.length;
        }
    }
    
    /**
     * Creates new PPPCode object from given integer locators and PPP. THe past PPP is not cloned.
     * @param locators as integer
     * @param ppp pivot permutation prefix
     */
    protected PPPCodeObject(int [] locators, final short [] ppp) {
        this.locators = locators;
        this.ppp = ppp;
    }
    
  
    // ***************     Access methods    ********************* //

//    void addLocators(int [] additionalLocators) {
//        ensureLocatorsRead();
//        int origLength = locators.length;
//        locators = Arrays.copyOf(locators, origLength + additionalLocators.length);
//        System.arraycopy(additionalLocators, 0, locators, origLength, additionalLocators.length);
//    }
    
    public short getIndexAt(int level) {
        return ppp[level];
    }
    
    public short[] getPppForReading() {
        return ppp;
    }

    public int getPPPLength() {
        return ppp.length;
    }
    
    public int [] getLocators() {
        return locators;
    }    

    public int [] getLocatorsClone() {
        return locators.clone();
    }    

    public int getLocatorCount() {
        return locators.length;
    }    
    
    @Override
    public String getLocatorURI() {
        return String.valueOf(Arrays.toString(locators));
    }
        
    
    // ************** Overridden methods - mostly to be removed with new MESSIF  ***************//
    @Override
    protected float getDistanceImpl(LocalAbstractObject obj, float distThreshold) {
        return LocalAbstractObject.UNKNOWN_DISTANCE;
    }

    @Override
    public float getMaxDistance() {
        return 1f;
    }

    @Override
    public int getSize() {
        return 0;
    }

    /**
     * Returns always <code>false</code> as there are no data.
     * @param obj object to compare this object with.
     * @return always false
     */
    @Override
    public boolean dataEquals(Object obj) {
        return equals(obj);
    }

    @Override
    public int dataHashCode() {
        return hashCode();
    }

    @Override
    protected void writeData(OutputStream stream) throws IOException {
    }

    @Override
    public String toString() {
        return "";
    }

    //************ New reading/writing to a byte buffer addressed by bits  ************//

    public PPPCodeObject(ByteBufferBits input, PPPCodeReadWriter pppCodeReader, int suffixLengthToRead, short [] pppUpper) throws BufferUnderflowException {
        this(input, pppCodeReader, suffixLengthToRead, suffixLengthToRead + pppUpper.length);
        System.arraycopy(pppUpper, 0, ppp, 0, pppUpper.length);
    }

    /**
     * Reads and create a PPP code object from specified byte buffer.
     * @param input byte buffer to read the data from (with internal position)
     * @param pppCodeReader efficient reader/writer from/to byte buffer
     * @param arrayLengthToRead length of the PPP array to be read and created
     * @throws BufferUnderflowException if the buffer does not have sufficient data to read the 
     */
    public PPPCodeObject(ByteBufferBits input, PPPCodeReadWriter pppCodeReader, int arrayLengthToRead) throws BufferUnderflowException {
        this(input, pppCodeReader, arrayLengthToRead, arrayLengthToRead);
    }

    /**
     * Reads and create a PPP code object from specified byte buffer.
     * @param input byte buffer to read the data from (with internal position)
     * @param pppCodeReader efficient reader/writer from/to byte buffer
     * @param arrayLengthToRead length of the PPP array to be read 
     * @param fullArrayLength length of the PPP array to be finally created
     * @throws BufferUnderflowException if the buffer does not have sufficient data to read the 
     */
    protected PPPCodeObject(ByteBufferBits input, PPPCodeReadWriter pppCodeReader, int arrayLengthToRead, int fullArrayLength) throws BufferUnderflowException {
        this.ppp = pppCodeReader.readPPP(input, arrayLengthToRead, fullArrayLength);
        this.locators = pppCodeReader.readIDArray(input);
    }
    
    /**
     * Given a {@link ByteBufferBits}, this method writes given PPP object to the buffer using 
     * given {@link PPPCodeReadWriter}.
     * @param output byte buffer to write to
     * @param pppCodeWriter efficient reader/writer from/to byte buffer
     * @throws BufferOverflowException if the buffer does not have sufficient capacity to hold the array
     */
    public void writeToBytes(ByteBufferBits output, PPPCodeReadWriter pppCodeWriter) throws BufferOverflowException {
        writeToBytes(output, pppCodeWriter, ppp.length);
    }
    
    /**
     * Given a {@link ByteBufferBits}, this method writes given PPP object to the buffer using 
     * given {@link PPPCodeReadWriter}.
     * @param output byte buffer to write to
     * @param pppCodeWriter efficient reader/writer from/to byte buffer
     * @param suffixLength suffix length of the PPP array to be written (the rest left-prefix is ignored)
     * @throws BufferOverflowException if the buffer does not have sufficient capacity to hold the array; 
     *   if thrown, the object was not written at all (not even partially)
     */
    public void writeToBytes(ByteBufferBits output, PPPCodeReadWriter pppCodeWriter, int suffixLength) throws BufferOverflowException {
        int positionBefore = output.positionInBits();
        try {
            if (locators.length > PPPCodeReadWriter.MAX_ARRAY_LENGTH) {
                int indexToWrite = 0;
                do {
                    writeToBytes(pppCodeWriter, output, suffixLength, Arrays.copyOfRange(locators, 
                            indexToWrite, Math.min(indexToWrite + PPPCodeReadWriter.MAX_ARRAY_LENGTH, locators.length)));
                    indexToWrite += PPPCodeReadWriter.MAX_ARRAY_LENGTH;
                }
                while (locators.length > indexToWrite);
            } else {
                writeToBytes(pppCodeWriter, output, suffixLength, locators);
            }
        } catch (BufferOverflowException ex) {
            output.positionInBits(positionBefore);
            throw ex;
        }
    }

    private void writeToBytes(PPPCodeReadWriter pppCodeWriter, ByteBufferBits output, int suffixLength, int [] locatorsToWrite) throws BufferOverflowException {
        pppCodeWriter.writePPP(output, ppp, suffixLength);
        pppCodeWriter.writeIDArray(output, locatorsToWrite);
    }
    
    /**
     * Returns the maximum binary bit size using given writer (counting together with boolean storage saying if several IDs
     *  are stored).
     * @param pppCodeReadWriter reader/writer to be used
     * @param tailLength length of the PPP of this object
     * @return the maximum binary bit size using given writer
     */
    public static int getBitsizeMax(PPPCodeReadWriter pppCodeReadWriter, int tailLength) {
        return pppCodeReadWriter.getPPPCodeBitSize(tailLength) + pppCodeReadWriter.getIDBitSize() + pppCodeReadWriter.getBoolBitSize();
    }    
    
}
