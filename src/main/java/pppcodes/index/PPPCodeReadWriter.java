/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.index;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.util.Arrays;
import mindex.distance.PartialQueryPPPDistanceCalculator;
import mindex.distance.QueryPPPDistanceCalculator;
import pppcodes.PPPCodeIndex;

/**
 * This class is meant to be created one for each PPP-Code M-Index and it
 *  writes and reads given PPP-Code objects to/from ByteBuffer. The write/read is
 *  as memory efficient as possible.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeReadWriter {

    public static final int BYTE_BITS = 8;
    
    /** Coefficient by which the average number of branching in the dynamic tree decreases with levels */
    protected static final float BRANCHING_SHRINK_COEF = 0.67f;
    
    /** Bit length of an integer representing a length of an array */
    public static final int ARRAY_LENGTH_BITS = 16;
    
    /** Maximal size of an integer array to be stored by this RW */
    public static final int MAX_ARRAY_LENGTH = (int) Math.pow(2, ARRAY_LENGTH_BITS) - 1;

    /** Minimum length of the ID array to use delta coding. */
    public static final int MIN_ARRAY_LENGTH_FOR_DELTA = 4;
    
    /** Number of pivots (n) in a single overlay */
    private final int nPivots;
    /** Number of bits necessary to store a pivot index (log_2 nPivots) */
    private final int nBitsForPivot;
    /** Number of bits to store integer identifier */
    private final int nBitsForID;
    
    
    // ***************************     Precomputed sizes and bit masks    ************************ //
    
    /** Bit masks for reading shorts from bytes */
    private final int[] MASKS;
    /** The i-th member of this bit mask has "i" ride-side ones and the rest are zeros. */
    private final int[] RIGHT_SIDE_ONES;
    /** The i-th member of this bit mask has "i" ride-side zeros and the rest are ones (within the lowest byte). */
    private final int[] RIGHT_SIDE_ZEROS;
    
    /** Maximum sizes (in bits) of PPPCode objects for different levels (0--maxLevel) */
    private final int [] PPPCODE_OBJ_SIZES_BITS;
    private final int [] MAX_BRANCHING;
    private final int [] BEST_LEAF_CAPACITIES;
    
    /**
     * Creates the writer given an M-Index configuration.
     * @param mIndex configuration of one M-Index overlay
     */
    public PPPCodeReadWriter(PPPCodeIndex mIndex) {
        this(mIndex.getNumberOfPivots(), mIndex.getMaxLevel(), mIndex.getMaxObjectNumber());
    }

    /**
     * Creates the writer given number of pivots in this M-Index overlay
     * @param numberOfPivots number of pivots in this M-Index overlay
     */
    public PPPCodeReadWriter(int numberOfPivots, short maxLevel, long maxObjectNumber) {
        super();
        this.nPivots = numberOfPivots;
        this.nBitsForPivot = getBitPrecisionForInt(nPivots - 1);
        int maxPivots = (int) Math.pow(2, nBitsForPivot);
        
        this.nBitsForID = getBitPrecisionForLong((maxObjectNumber <= 0) ? Integer.MAX_VALUE : maxObjectNumber);

        // initialize bit masks and other precomputed numbers
        this.MASKS = new int [BYTE_BITS + 1];
        this.RIGHT_SIDE_ZEROS = new int [BYTE_BITS + 1];
        this.RIGHT_SIDE_ONES = new int [BYTE_BITS + 1];
        for (int i = 0; i <= BYTE_BITS; i++) {
            MASKS[i] = (maxPivots - 1) << i;
            RIGHT_SIDE_ONES[i] = 255 >>> (BYTE_BITS - i);
            RIGHT_SIDE_ZEROS[i] = 255 << i;
        }
        
        MAX_BRANCHING = new int [maxLevel + 1];
        MAX_BRANCHING[0] = nPivots;
        for (int i = 1; i < maxLevel + 1; i++) {
            MAX_BRANCHING[i] = (int) Math.ceil(MAX_BRANCHING[i - 1] * BRANCHING_SHRINK_COEF);            
        }
        
        PPPCODE_OBJ_SIZES_BITS = new int [maxLevel + 1];
        BEST_LEAF_CAPACITIES = new int [maxLevel + 1];
        for (int i = 0; i < maxLevel + 1; i++) {
            PPPCODE_OBJ_SIZES_BITS[i] = PPPCodeObject.getBitsizeMax(this, maxLevel - i);
            BEST_LEAF_CAPACITIES[i] = (int) (((MAX_BRANCHING[i] * PPPCodeLeafCell.LEAF_SIZE_BITS / nBitsForPivot) * PPPCODE_OBJ_SIZES_BITS[i]) / 8f) ;
        }        
    }
    
    // *****************      Read and write methods for the PPPCode objects     ********************** //
    
    /**
     * Get number of bits for any {@link PPPCodeObject} serialized by this serializer.
     * @param level level of the leaf in which this object is to be stored
     * @return number of bits for the specified {@link PPPCodeObject}
     */
    public final int getPPPCodeObjectBitSize(int level) {
        return PPPCODE_OBJ_SIZES_BITS[level];
    }
    
 
    /**
     * Returns the number of bits that would carry an array of specified length.
     * @param arrayLength length of array
     * @return number of bits that would carry an array of specified length
     */
    public int getPPPCodeBitSize(final int arrayLength) {
        return (nBitsForPivot * arrayLength);
    }

    /**
     * Get the number of bits that an integer number would occupy.
     * @return number of bits that an integer number would occupy.
     */
    public int getIDBitSize() {
        return nBitsForID;
    }
    
    public int getBoolBitSize() {
        return 1;
    }
    
    //   *****************************     New way of writing/reading to buffer indexed at the level of bits     ********************* //

    /** 
     * Universal method for reading and creating PPPCodeObjects. It has the following variants:
     * <ul><li>if {@code calculator} is null, then standard {@link PPPCodeObject} is created</li>
     *     <li>if {@code calculator} is null and {@code pppUpper} is not null, then {@link PPPCodeObject} with upper PPP set to {@code pppUpper} is created</li>
     *     <li>if {@code calculator} is NOT subtype of {@link PartialQueryPPPDistanceCalculator} then pppUpper is used to construct {@link PPPCodeObjectDistance}</li>
     *     <li>if {@code calculator} is subtype of {@link PartialQueryPPPDistanceCalculator} then {@code upperLength} and {@code topDistance} are used to construct {@link PPPCodeObjectDistance}</li>
     * </ul>
     * @param input
     * @param arrayLength
     * @param calculator
     * @param pppUpper
     * @param upperLength
     * @param topDistance
     * @return
     * @throws BufferUnderflowException 
     */
    public PPPCodeObject readObject(ByteBufferBits input, int arrayLength, QueryPPPDistanceCalculator calculator, short [] pppUpper, int upperLength, float topDistance) throws BufferUnderflowException {
        if (calculator == null) {
            if (pppUpper == null) {
                return new PPPCodeObject(input, this, arrayLength);
            }
            return new PPPCodeObject(input, this, arrayLength, pppUpper);
        }
        if (calculator instanceof PartialQueryPPPDistanceCalculator) {
            return new PPPCodeObjectDistance(input, this, arrayLength, (PartialQueryPPPDistanceCalculator) calculator, upperLength + 1, topDistance);
        }
        return new PPPCodeObjectDistance(input, this, arrayLength, calculator, pppUpper);
    }
    
    protected void writePPP(ByteBufferBits outputBuffer, short[] array, int tailLength) throws BufferOverflowException {
        try {
            for (; tailLength > 0; tailLength--) {                
                outputBuffer.put(array[array.length - tailLength], nBitsForPivot);
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new BufferOverflowException();
        }
    }

    protected short [] readPPP(ByteBufferBits inputBuffer, int suffixLengthToRead, int fullArrayLength) throws BufferUnderflowException {
        try {
            short[] ppp = new short[fullArrayLength];

            // iterate over all numbers to be created and build them bit by bit
            for (int i = fullArrayLength - suffixLengthToRead; i < fullArrayLength; i++) {
                ppp[i] = (short) inputBuffer.get(nBitsForPivot);
            }
            return ppp;
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new BufferUnderflowException();
        }
    }

    /**
     * Reads only the first index from the stored PPP and skip the rest of the PPP and all IDs stored for this PPP.
     * @param input input buffer (with position)
     * @param pppLengthStored the length of the PPP stored in the input buffer
     * @return the read first PPP index
     * @throws BufferUnderflowException if the input buffer does not contain enough data 
     */
    protected int readNextLevelPivot(ByteBufferBits input, int pppLengthStored) throws BufferUnderflowException {
        try {
            int retVal = input.get(nBitsForPivot);
            // skip the rest of the PPP
            input.skip((pppLengthStored - 1) * nBitsForPivot);
            
            skipIDs(input);
            return retVal;
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new BufferUnderflowException();
        }
    }
    
    void writeIDInt(ByteBufferBits output, int value) throws BufferOverflowException {
        writeInt(output, value, nBitsForID);
    }    

    int readIDInt(ByteBufferBits input) throws BufferUnderflowException {
        return readInt(input, nBitsForID);
    }

    /**
     * Writes given array of IDs to given buffer; the IDs are not necessarily
     *  written (and read) in the same order! There are three ways to store the array:
     * <ul><li>array of length 1 is stored as "one bit boolean 'true' and the ID"</li>
     * <li>array of length 2, 3 is stored as "length of array and the IDs"</li>
     * <li>array of length >= 4 is stored by delta encoding of the IDs</li></ul>
     * @param output output buffer
     * @param values list of IDs to be written
     * @throws BufferOverflowException if size of the buffer was exceeded
     */
    void writeIDArray(ByteBufferBits output, int [] values) throws BufferOverflowException {
        writeBool(output, values.length != 1);
        if (values.length == 1) {
            writeIDInt(output, values[0]);
            return;
        }
        if (values.length > MAX_ARRAY_LENGTH) {
            throw new IllegalArgumentException("this R/W can write only arrays up to length " + MAX_ARRAY_LENGTH+": " + values.length);
        }
        writeInt(output, values.length, ARRAY_LENGTH_BITS);
        
        // if the size of the array is larger then a constant, then the Delta coding is used
        if (values.length < MIN_ARRAY_LENGTH_FOR_DELTA) {
            for (int i : values) {
                writeIDInt(output, i);
            }
        } else {
            writeIDArrayDelta(output, values);
        }
    }    

    private void writeIDArrayDelta(ByteBufferBits output, int[] values) throws BufferOverflowException {
        // sort the arrays and calculate the differences (and find out the largest difference)
        Arrays.sort(values);
        int [] deltas = new int [values.length];
        int maxDelta = 0;
        for (int i = 1; i < values.length; i++) {
            deltas[i] = values[i] - values[i-1];
            if (maxDelta < deltas[i]) {
                maxDelta = deltas[i];
            }
        }
        // write the bitsize of the maximal delta (necessary for reading)
        int deltaBitPrecision = getBitPrecisionForInt(maxDelta);
        writeInt(output, deltaBitPrecision, nBitsForID);
        // write the first (smallest) ID
        writeIDInt(output, values[0]);
        // write the deltas
        for (int i = 1; i < deltas.length; i++) {
            writeInt(output, deltas[i], deltaBitPrecision);
        }
    }

    /**
     * Reads and returns an array of IDs from given input buffer.
     * @param input buffer to read the array from
     * @return the read and created array of IDs
     * @throws BufferUnderflowException 
     */
    protected int [] readIDArray(ByteBufferBits input) throws BufferUnderflowException {
        if (! readBool(input)) {
            return new int [] { readIDInt(input) };
        }
        int arraySize = readInt(input, ARRAY_LENGTH_BITS);
        // TODO: remove when indexes rebuilt
        if (arraySize == 0) {
            arraySize = MAX_ARRAY_LENGTH + 1;
        }
        int [] retVal = new int [arraySize];
        if (retVal.length < MIN_ARRAY_LENGTH_FOR_DELTA) {
            for (int i = 0; i < retVal.length; i++) {
                retVal[i] = readIDInt(input);
            }
        } else {
            readIDArrayDelta(input, retVal);
        }
        return retVal;
    }

    private void readIDArrayDelta(ByteBufferBits input, int[] retVal) throws BufferUnderflowException {
        try {
            // read the bitsize of the deltas
            int deltaBitPrecision = readInt(input, nBitsForID);
            // read the first ID (full)
            retVal[0] = readIDInt(input);
            // read the deltas and reconstruct
            for (int i = 1; i < retVal.length; i++) {
                retVal[i] = retVal[i - 1] + readInt(input, deltaBitPrecision);
            }
        } catch (BufferUnderflowException e) {
            //MetricIndexes.logger.warning("buffer underflow when reading IDs from leaf buffer: " + e.getMessage());
            throw e;
        }
    }
    
    /**
     * Skip the stored IDs in whatever format
     * @param input
     * @return
     * @throws BufferUnderflowException
     * @throws ArrayIndexOutOfBoundsException 
     */
    void skipIDs(ByteBufferBits input) throws BufferUnderflowException, ArrayIndexOutOfBoundsException {
        // skip the IDs stored for this PPP
        if (! readBool(input)) {
            // only one ID is stored
            input.skip(nBitsForID);
        } else {
            int idArraySize = input.get(ARRAY_LENGTH_BITS);
            // TODO: remove when indexes rebuilt
            if (idArraySize == 0) {
                idArraySize = MAX_ARRAY_LENGTH + 1;
            }
            if (idArraySize < MIN_ARRAY_LENGTH_FOR_DELTA) {
                input.skip(idArraySize * nBitsForID);
            } else {
                // read the bitsize of the deltas
                int deltaBitPrecision = input.get(nBitsForID);
                // skip the first ID (full)
                input.skip(nBitsForID);
                // read the deltas and reconstruct
                input.skip((idArraySize - 1) * deltaBitPrecision);
            }
        }
    }

    
    private void writeInt(ByteBufferBits output, int value, int precision) throws BufferOverflowException {
        try {
            output.put(value, precision);
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new BufferOverflowException();
        }            
    }    

    private int readInt(ByteBufferBits input, int precision) throws BufferUnderflowException {
        try {
            return input.get(precision);
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new BufferUnderflowException();
        }
    }
    
    void writeBool(ByteBufferBits output, boolean value) throws BufferOverflowException {
        try {
            output.put(value ? 1 : 0, 1);
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new BufferOverflowException();
        }            
    }    

    boolean readBool(ByteBufferBits input) throws BufferUnderflowException {
        try {
            return input.get(1) == 1;
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new BufferUnderflowException();
        }
    }
    
    /**
     * Go throw the whole passed ByteBuffer and read first pivot indexes from the stored
     *  PPPs. These first indexes are returned in a set.
     */
    public boolean[] readNextLevelPivots(ByteBufferBits input, int pppLength) {
        if (pppLength <= 0) {
            return null;
        }
        boolean[] retVal = new boolean[nPivots];
        while (input.positionInBits() < input.limitInBits()) {
            retVal[readNextLevelPivot(input, pppLength)] = true;
        }
        return retVal;
    }
    
    public float getMinPPPDistance(ByteBufferBits input, int arrayLength, PartialQueryPPPDistanceCalculator calculator, int upperLength) throws BufferUnderflowException {
        try {
            float minPPPDistance = Float.MAX_VALUE;
            while (input.positionInBits() < input.limitInBits()) {
                minPPPDistance = Math.min(minPPPDistance, calculator.getPartialQueryDistance(readPPP(input, arrayLength, arrayLength), upperLength + 1));
                skipIDs(input);
            }
            return minPPPDistance;
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new BufferUnderflowException();
        }
    }
    
    /**
     *
     * @param data
     * @return
     */
    public ByteBufferBits createNewBuffer(byte [] data) {
        return new WriterByteBufferBits(data);
    }

    public ByteBufferBits createNewBuffer(byte [] data, int limitInBits) {
        return new WriterByteBufferBits(data, limitInBits);
    }

    public ByteBufferBits createNewBuffer(int positionInBits, byte [] data) {
        return new WriterByteBufferBits(positionInBits, data);
    }

    /**
     * Given a value, this method returns the number of bits necessary to represent the number.
     * @param value number
     * @return the number of bits necessary to represent the number
     */
    protected static int getBitPrecisionForLong(long value) {
        return Long.SIZE-Long.numberOfLeadingZeros(value);
    }
    
    /**
     * Given a value, this method returns the number of bits necessary to represent the number.
     * @param value number
     * @return the number of bits necessary to represent the number
     */
    protected static int getBitPrecisionForInt(int value) {
        return Integer.SIZE-Integer.numberOfLeadingZeros(value);
    }    
    /**
     * Implementation of the byte buffer indexed at the level of bits.
     */
    protected class WriterByteBufferBits implements PackageByteBufferBits {

        /** Byte buffer data */
        byte [] data;

        /** Current position in the data buffer */
        int positionBits;
        
        /** Limit of this buffer in bits */
        int limitBits;

        public WriterByteBufferBits(byte[] data) {
            this (data, data.length * BYTE_BITS);
        }

        public WriterByteBufferBits(byte[] data, int limitInBits) {
            this(data, limitInBits, 0);
        }

        public WriterByteBufferBits(int positionBits, byte[] data) {
            this(data, data.length * BYTE_BITS, positionBits);
        }
        
        protected WriterByteBufferBits(byte[] data, int limitInBits, int positionBits) {
            this.data = data;
            this.limitBits = limitInBits;
            this.positionBits = positionBits;
        }
        
        @Override
        public int get(int bits) throws ArrayIndexOutOfBoundsException {
            int retVal = 0; // created value to be returned            
            int bitsInByte = BYTE_BITS - (positionBits % BYTE_BITS);
            while (bits > 0) {
                int bitsToReadNow = Math.min(bits, bitsInByte);
                retVal = retVal << bitsToReadNow | (((int) (RIGHT_SIDE_ONES[bitsInByte] & data[positionBits / BYTE_BITS])) >>> Math.max(0, bitsInByte-bits));
                bits -= bitsToReadNow;
                positionBits += bitsToReadNow;
                bitsInByte = BYTE_BITS;
            }
            return retVal;
        }
        
        @Override
        public void put(int value, int bits) throws ArrayIndexOutOfBoundsException {
            int bitsAvlInByte = BYTE_BITS - (positionBits % BYTE_BITS);
            while (bits > 0) {
                int bitsToWriteNow = Math.min(bits, bitsAvlInByte);
                // find out, if the current stored number fits into current byte and how much it should be shifted
                int bitsShift = bits - bitsAvlInByte;
                data[positionBits / BYTE_BITS] = (byte) ((data[positionBits / BYTE_BITS] & RIGHT_SIDE_ZEROS[bitsAvlInByte]) | (byte) (0xFF & ((bitsShift > 0) ? 
                            // if current number does not fit into the byte, shift the bits right
                            (value >>> bitsShift) : 
                            // if current numbers fits into the byte, shift its bits left
                            (value << -bitsShift))));
                bits -= bitsToWriteNow;
                positionBits += bitsToWriteNow;
                bitsAvlInByte = BYTE_BITS;
            }
        }

        @Override
        public void skip(int bits) throws ArrayIndexOutOfBoundsException {
            if (positionBits + bits > limitBits) {
                throw new ArrayIndexOutOfBoundsException("Capacity (limit) of this buffer is smaller then the new position: " + positionBits + bits);
            }
            positionBits += bits;
        }

        @Override
        public int position() {
            return PPPCodeReadWriter.bitsToBytes(positionBits);
        }

        @Override
        public int positionInBits() {
            return positionBits;
        }

        @Override
        public void positionInBits(int newPositionBits) {
            this.positionBits = newPositionBits;
        }
        
        @Override
        public int limit() {
            return PPPCodeReadWriter.bitsToBytes(limitBits);
        }

        @Override
        public int limitInBits() {
            return limitBits;
        }

        @Override
        public void limitInBits(int newLimitBit) {
            this.limitBits = newLimitBit;
        }

        @Override
        public byte[] getBuffer() {
            return data;
        }
    }
    
    
    // ************************************        Leaf capacity and branching       ********************************* //

    /**
     * Returns the capacity (in bytes) of a leaf cluster in PPP-Code tree so that the storage is as efficient as possible.
     */
    public int getOptimalLeafCapacityBytes(final short leafLevel) {
        //return (MAX_BRANCHING[leafLevel] * PPPCodeLeafCell.LEAF_SIZE_BITS / nBitsForPivot) * getPPPCodeObjectBinarySize(leafLevel);
        return BEST_LEAF_CAPACITIES[leafLevel];
    }

    public int getOptimalLeafCapacityBytes(int branching, short leafLevel) {
        return (branching > 0) ? (int) (((branching * PPPCodeLeafCell.LEAF_SIZE_BITS / nBitsForPivot) * PPPCODE_OBJ_SIZES_BITS[leafLevel]) / 8f) 
                : BEST_LEAF_CAPACITIES[leafLevel];
    }
    
    int getMaxBranching(int level) {
        return MAX_BRANCHING[level];
    }

    
    // **************************     Static auxiliary methods      *************************** //
    
    public static int bitsToBytes(int bitsOccupation) {
        if (bitsOccupation == 0) {
            return 0;
        }
        return (bitsOccupation + (BYTE_BITS - 1)) / (BYTE_BITS);
    }
    
}
