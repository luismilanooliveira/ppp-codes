/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes;

import pppcodes.index.PPPCodeObject;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.index.impl.DiskStorageMemoryIntIndex;
import messif.objects.LocalAbstractObject;
import messif.objects.keys.IntegerKey;
import mindex.MIndexProperties;
import mindex.MIndexStdInitializer;
import pppcodes.ids.DirectStringIntConvertor;
import pppcodes.ids.LocatorStringIntConvertor;

/**
 * This is class is a simple extension of {@link MIndexPPCalculator}. It can
 * calculate PP of given objects or a collection of objects using a thread pool.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeInitializer extends MIndexStdInitializer {

    /** Class serial ID for serialization. */
    private static final long serialVersionUID = 114091L;
    
    /** The string-integer locator transformer used when the standard data object is transformed to {@link PPPCodeObject}. */
    protected LocatorStringIntConvertor convertor;
    
    /**
     * Creates the PP calculator given M-Index properties
     *
     * @param mIndexProperties properties with M-Index configuration
     * @throws InstantiationException if this part of M-Index cannot be
     * initialized using this properties
     */
    public PPPCodeInitializer(MIndexProperties mIndexProperties) throws InstantiationException {
        this(mIndexProperties, new DirectStringIntConvertor());
    }

    /**
     * Creates the PP calculator given M-Index properties
     *
     * @param mIndexProperties properties with M-Index configuration
     * @param convertor string-integer locator transformer used when the standard data object is transformed to {@link PPPCodeObject}
     * @throws InstantiationException if this part of M-Index cannot be
     * initialized using this properties
     */
    public PPPCodeInitializer(MIndexProperties mIndexProperties, LocatorStringIntConvertor convertor) throws InstantiationException {
        super(mIndexProperties);
        this.convertor = convertor;
    }
    
    /** 
     * Sets the string-integer locator convertor (common for the whole PPP-Code structure).
     * @param convertor new convertor
     */
    public void setConvertor(LocatorStringIntConvertor convertor) {
        this.convertor = convertor;
    }    
    
    // *********************  Getter overrides    ************************ //
    
    @Override
    public boolean isUsePivotFiltering() {
        return false;
    }

    @Override
    public boolean isCheckDuplicateDC() {
        return false;
    }    
    
    @Override
    public boolean isPreciseSearch() {
        return false;
    }
    
    
    // *************** Init object - calculate obj-pivot distance + PP ********************* //
    
    /**
     * Initializes the object to be inserted into the PPP-Code single algorithm. The passed data object is
     *  transformed into a {@link PPPCodeObject}.
     *
     * @param object an object to be initialized
     * @return a new  {@link PPPCodeObject}.
     * @throws messif.algorithms.AlgorithmMethodException if anything goes wrong
     */
    @Override
    public LocalAbstractObject initPP(LocalAbstractObject object) throws AlgorithmMethodException {
        // fallback for the variant with internal ID-object refinement storage
        if (object.getObjectKey() instanceof IntegerKey) {
            return new PPPCodeObject(object.getObjectKey(), DiskStorageMemoryIntIndex.getIntegerID(object), getObjectPPP(object));
        }
        return new PPPCodeObject(object.getObjectKey(), convertor.getIntLocator(object.getLocatorURI()), getObjectPPP(object));
    }
    
}
