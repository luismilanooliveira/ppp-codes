/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes;

import java.util.Arrays;
import pppcodes.index.PPPCodeInternalCell;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.NavigationProcessor;
import messif.buckets.BucketDispatcher;
import messif.objects.LocalAbstractObject;
import messif.operations.AbstractOperation;
import messif.operations.data.BulkInsertOperation;
import messif.operations.data.DataManipulationOperation;
import messif.operations.data.DeleteOperation;
import messif.operations.data.InsertOperation;
import mindex.MIndexObjectInitilizer;
import mindex.MIndexProperties;
import mindex.MetricIndex;
import mindex.navigation.VoronoiInternalCell;
import mindex.processors.DataManipulationNavigationProcessor;
import pppcodes.index.PPPCodeObject;
import pppcodes.index.PPPCodeReadWriter;

/**
 * The full configuration and some functionality of one PPP-Code tree index. It extends the M-Index because
 *  it shares many common features and code.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeIndex extends MetricIndex {
    
    /** Class serial ID for serialization */
    private static final long serialVersionUID = 112300L;
    
    /** PPP Code serializer - it can be null */
    protected transient PPPCodeReadWriter pppCodeSerializer;
    
    //************************************   Getters ************************//
    
    /**
     * Returns a serializer for PPP Codes.
     * @return serializer for PPP Codes
     */
    public final PPPCodeReadWriter getPPPCodeReadWriter() {
        if (pppCodeSerializer == null) {
            pppCodeSerializer = new PPPCodeReadWriter(this);
        }
        return pppCodeSerializer;
    }
    
    @Override
    public boolean isSpecialOverfilledBuckets() {
        return false;
    }
        
    @Override
    public boolean isApproxProcessWholeMultiBucket() {
        return false;
    }

    @Override
    public boolean isApproxCheckKeyInterval() {
        return false;
    }
    
    @Override
    public boolean isMultiBuckets() {
        return false;
    }
        
    @Override
    public boolean isPreciseSearch() {
        return false;
    }

    /**
     * TODO: read this either from config and/or from the LocatorID convertor
     * @return 
     */
    public long getMaxObjectNumber() {        
        return Integer.MAX_VALUE;
//        return configuration.getLongProperty(MIndexProperty.MAX_OBJECT_NUMBER);
    }
    
        
    // **************************************    Initialization   ************************************* //

    /**
     * Creates new instance of M-Index having a file name of M-Index properties.
     * @param origConfiguration created properties specifying M-Index configuration
     * @param prefix prefix of the properties (e.g. "mindex.") for this PPP-Code single index
     * @throws java.lang.InstantiationException
     */
    public PPPCodeIndex(Properties origConfiguration, String prefix) throws InstantiationException, AlgorithmMethodException {
        this( new MIndexProperties(origConfiguration, prefix));
    }
    
    protected PPPCodeIndex(MIndexProperties config) throws InstantiationException, AlgorithmMethodException {
        super(config);
    }

    @Override
    protected VoronoiInternalCell initVoronoiCellTree() throws AlgorithmMethodException {
        return new PPPCodeInternalCell(this, null, new short[0]) ;
    }

    @Override
    protected MIndexObjectInitilizer initPPCalculator(MIndexProperties config) throws InstantiationException {
        return new PPPCodeInitializer(config);
    }

    @Override
    protected BucketDispatcher initBucketDispatcher(MIndexProperties config) throws InstantiationException {
        return null;
    }

    @Override
    @SuppressWarnings({"FinalizeDeclaration", "FinalizeCalledExplicitly"})
    public void finalize() throws Throwable {
        // NEVER PUT CONSOLIDATE DATA IN FINALIZE, BECAUSE IT WOULD BE CALLED AFTER THE SERIALIZATION
        super.finalize();
    }
    
    // *********************  Methods taking care about the storage and dynamic levels of M-Index  ********************** //
        
    /**
     * Return the number of objects stored by this M-Index.
     * @return the number of objects stored by this M-Index.
     */
    @Override
    public int getObjectCount() {
        return voronoiCellTree.getObjectCount();
    }

    /**
     * Return various statistic numbers about this PPP-Code single index.
     */
    public void getTreeSize(Map<Short,AtomicInteger> intCellNumbers, Map<Short, AtomicLong> branchingSums, AtomicInteger leafCellNumber, AtomicLong dataSizeBytes, AtomicLong leafLevelSum) {
        voronoiCellTree.calculateTreeSize(intCellNumbers, branchingSums, leafCellNumber, dataSizeBytes, leafLevelSum);
    }

    /**
     * Consolidate data in all leaf nodes of the dynamic Voronoi tree by putting together object IDs with the same PPP.
     */
    public void consolidateTreeData() {
        ((PPPCodeInternalCell) voronoiCellTree).consolidateData(new byte[1024 * 1024]);
    }

    @Override
    public short [] readPPP(LocalAbstractObject object) {
        if (object instanceof PPPCodeObject) {
            return ((PPPCodeObject) object).getPppForReading();
        }
        return super.readPPP(object);
    }
    
    
    // *******************************   Methods processing operations *************************************** //
    
    /** Pre-created list of supported operations. */
    private final static List<Class<? extends AbstractOperation>> supportedOperations = 
            Collections.unmodifiableList((List) Arrays.asList(BulkInsertOperation.class, InsertOperation.class, DeleteOperation.class));
    
    @Override
    public List<Class<? extends AbstractOperation>> getSupportedOpList() {
        return PPPCodeIndex.supportedOperations;
    }

    /**
     * Method that creates specific implementation of NavigationProcessor for  each type of operation - a single
     *  PPP-Code index can process only the insert/delete/update operations.
     * @param operation to be processed
     * @return a specific implementation of NavigationProcessor
     */
    @Override
    public NavigationProcessor<? extends AbstractOperation> getNavigationProcessor(AbstractOperation operation) {
        try {
            // For InsertOperation, BulkInsertOperation and DeleteOperation
            if (operation instanceof DataManipulationOperation) {
                return DataManipulationNavigationProcessor.getInstance(operation, this);
            }
        } catch (AlgorithmMethodException e) {
            throw new IllegalStateException(e);
        }
        throw new UnsupportedOperationException("PPP-Code single algorithm does not support processing of operation " + operation.getClass());
    }

    
    // *********************************************   Auxilionary methods   ********************************* //

    /** 
     * Prints info about this M-Index.
     * @return string representation of this M-Index
     */
    @Override
    public String toString() {
        StringBuilder strBuf = new StringBuilder("Single PPP-Code algorithm: \n");
        for (String property : configuration.stringPropertyNames()) {
            strBuf.append("\t").append(property).append(" = ").append(configuration.getProperty(property)).append('\n');
        }
        // pivot settings information
        strBuf.append(ppCalculator.toString());
        
        // storage information
        strBuf.append("\nStoring: ").append(getObjectCount()).append(" objects\n");
        return strBuf.toString();
    }
    
}
