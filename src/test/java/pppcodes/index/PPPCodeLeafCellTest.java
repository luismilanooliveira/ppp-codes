/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pppcodes.index;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import messif.algorithms.AlgorithmMethodException;
import mindex.navigation.VoronoiInternalCell;
import org.jmock.Expectations;
import static org.jmock.Expectations.returnValue;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Test;
import org.junit.runner.RunWith;
import pppcodes.PPPCodeIndex;

/**
 *
 * @author david
 */
@RunWith(JMock.class)
public class PPPCodeLeafCellTest {
    
    public PPPCodeLeafCellTest() {
    }

    /**
     * Test of addLocatorToDeleted method, of class PPPCodeLeafCell.
     */
    @Deprecated
    public void testAddLocatorToDeleted() {
        System.out.println("testAddLocatorToDeleted");
        PPPCodeLeafCell instance = new PPPCodeLeafCell(null, null);
        for (int value : new int [] {4, 6, 1, 4, 2, -1, 9, 0, 5, 6}) {
            boolean allDeleted = false;
            while (! allDeleted) {
                try {
                    instance.addDeletedIndex(value);
                    allDeleted = true;
                } catch (IllegalArgumentException e) {
                    instance.consolidateData(null);
                }            
            }
            System.out.println("current: " + Arrays.toString(instance.deletedIndexes));
        }
    }
    
    private Mockery context = new JUnit4Mockery() {
            {
                setImposteriser(ClassImposteriser.INSTANCE);
            }
        };    
    private PPPCodeIndex mIndexMock;

    private PPPCodeLeafCell initData() throws AlgorithmMethodException {
        
        final VoronoiInternalCell parent = context.mock(VoronoiInternalCell.class);
        mIndexMock = context.mock(PPPCodeIndex.class);
        context.checking(new Expectations() {
            {
                allowing(parent).getLevel();
                will(returnValue((short) 2));
                allowing(parent).getChildPPP(null);
                will(returnValue(new short [] {(short) 1, (short) 2, (short) 3}));
                allowing(mIndexMock).getMaxLevel();
                will(returnValue((short) 4));
                allowing(mIndexMock).getNumberOfPivots();
                will(returnValue((short) 100));
                allowing(mIndexMock).getMaxObjectNumber();
                will(returnValue((long) 0));
                allowing(mIndexMock).getPPPCodeReadWriter();
                will(returnValue(new PPPCodeReadWriter(100, (short) 6, 0L)));
            }
        });
        
        
        PPPCodeLeafCell instance = new PPPCodeLeafCell(mIndexMock, parent);
        // insert data
        PPPCodeObject pppCodeObject = new PPPCodeObject(new int [] {312, 12, 3, 4, 1, 90}, new short [] {(short) 1, (short) 2, (short) 3, (short) 4});
        instance.insertObjects(Collections.singletonList(pppCodeObject));
        System.out.println("1st object: " + Arrays.toString(pppCodeObject.getLocators()));
//        pppCodeObject = new PPPCodeObject(new int [] {12, 212, 13, 44, 144, 0}, new short [] {(short) 1, (short) 2, (short) 3, (short) 5});
        pppCodeObject = new PPPCodeObject(new int [] {144}, new short [] {(short) 1, (short) 2, (short) 3, (short) 5});
        instance.insertObjects(Collections.singletonList(pppCodeObject));
        System.out.println("2nd object: " + Arrays.toString(pppCodeObject.getLocators()));

        return instance;
    }
    
    private void printContent(PPPCodeLeafCell instance) {
        System.out.println("current data:");
        for (Iterator<PPPCodeObject> iterator = instance.getAllObjects(); iterator.hasNext();) {
            System.out.println(Arrays.toString(iterator.next().getLocators()));
        }
    }
    
    @Test
    public void testRemove() throws AlgorithmMethodException {
        System.out.println("testConsolidateAfterRemove");
        PPPCodeLeafCell instance = initData();
        printContent(instance);
        for (int value : new int [] {0, 0, 3, 3, 0, 6}) {
            boolean allDeleted =false;
            while (! allDeleted) {
                try {
                    instance.addDeletedIndex(value);
                    allDeleted = true;
                } catch (IllegalArgumentException e) {
                    instance.consolidateData(null);
                }            
            }            
            printContent(instance);
        }
        instance.consolidateData(null);
        printContent(instance);
    }
    
    @Test
    public void testDeleteAndInsert() throws AlgorithmMethodException {
        System.out.println("testDeleteAndInsert");
        PPPCodeLeafCell instance = initData();
        printContent(instance);
        
        // remove one object
        PPPCodeObject objToRemove = new PPPCodeObject(new int [] {144}, new short [] {(short) 1, (short) 2, (short) 3, (short) 5});
        instance.deleteObjects(Collections.singletonList(objToRemove), 0, true);
        printContent(instance);
        
        instance.consolidateData(null);
        printContent(instance);
        
        instance.insertObjects(Collections.singletonList(objToRemove));
        printContent(instance);
        
        instance.consolidateData(null);
        printContent(instance);
    }
        
    
//    /**
//     * Test of consolidateData method, of class PPPCodeLeafCell.
//     */
//    @Test
//    public void testConsolidateData() {
//        System.out.println("consolidateData");
//        byte[] temporary = null;
//        PPPCodeLeafCell instance = null;
//        instance.consolidateData(temporary);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
