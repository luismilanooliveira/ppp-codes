/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.index;

import pppcodes.index.PPPCodeReadWriter;
import pppcodes.index.PPPCodeObject;
import pppcodes.index.ByteBufferBits;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import junit.framework.TestCase;
import org.junit.Test;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeSerializatorTest extends TestCase {
    
    public PPPCodeSerializatorTest(String testName) {
        super(testName);
    }
    
    PPPCodeReadWriter serializator = null;
        
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        buffer = new byte[409600];
//        writeBuffer = new serializator.WriterByteBufferBits(buffer);
//        writeBuffer = ByteBuffer.allocate(4096);  
//        output = new BufferOutputStream(buffer);
//        input = new BufferInputStream(buffer);
//        serializator = new PPPCodeReadWriter(32);
    }

    private byte [] buffer;
//    private ByteBufferBits writeBuffer;
//    private BufferOutputStream output;
//    private BufferInputStream input;
    private Random random = new Random(System.currentTimeMillis());
    
    public void testWriteAndRead() throws Exception {
        System.out.println("write and read PPP");        
        for (short maxPivots = 16; maxPivots <= 1024; maxPivots *= 2) {
            for (short pppLength = 0; pppLength <= 16; pppLength++) {
                short[] array = new short[pppLength];
                for (int i = 0; i < array.length; i++) {
                    array[i] = (short) random.nextInt(maxPivots);
                }
                //short[] array = new short[] {(short) 3, (short) 2, (short) 1, (short) 0};
                PPPCodeObject pppCodeObject = new PPPCodeObject(Math.abs(random.nextInt()), array);
                System.out.println("testing number of pivots: " + maxPivots);
                serializator = new PPPCodeReadWriter(maxPivots, pppLength, 0L);
                ByteBufferBits writeBuffer = serializator.createNewBuffer(buffer);
                pppCodeObject.writeToBytes(writeBuffer, serializator, pppLength);
                PPPCodeObject readObject = serializator.readObject(serializator.createNewBuffer(buffer), pppLength, null, null, 0, 0f);
                assertEquals(pppCodeObject.getLocators()[0], readObject.getLocators()[0]);
                for (int j = 0; j < pppLength; j++) {
                    assertEquals(pppCodeObject.getPppForReading()[j], readObject.getPppForReading()[j]);
                }            

            }
        }
    }

    @Test
    public void testWriteAndReadNew() throws Exception {
        long MAX_OBJECTS = 100000000L;
        System.out.println("write and read PPP by the new method");        
        for (short maxPivots = 16; maxPivots <= 1024; maxPivots *= 2) {
            for (short pppLength = 0; pppLength <= 16; pppLength++) {
                System.out.println("testing number of pivots: " + maxPivots);
                serializator = new PPPCodeReadWriter(maxPivots, pppLength, MAX_OBJECTS);
                ByteBufferBits writeBufferBits = serializator.createNewBuffer(buffer);
                List<PPPCodeObject> writtenObjects = new ArrayList<>(100);
                for (int j = 0; j < 10; j++) {
                    short[] array = new short[pppLength];
                    for (int i = 0; i < array.length; i++) {
                        array[i] = (short) random.nextInt(maxPivots);
                    }
                    int [] ids = new int [random.nextInt(256) + 1];
                    for (int i = 0; i < ids.length; i++) {
                        ids[i] = Math.abs(random.nextInt((int) MAX_OBJECTS));
                    }
                    PPPCodeObject pppCodeObject = new PPPCodeObject(ids, array);
                    pppCodeObject.writeToBytes(writeBufferBits, serializator, pppLength);
                    writtenObjects.add(pppCodeObject);
                }
                
                ByteBufferBits readBufferBits = serializator.createNewBuffer(buffer);
                for (int j = 0; j < 10; j++) {
                    PPPCodeObject pppCodeObject = writtenObjects.get(j);
                    PPPCodeObject readObject = new PPPCodeObject(readBufferBits, serializator, pppLength);
                    assertTrue(Arrays.equals(pppCodeObject.getLocators(), readObject.getLocators()));
                    for (int i = 0; i < pppLength; i++) {
                        assertEquals(pppCodeObject.getPppForReading()[i], readObject.getPppForReading()[i]);
                    }
                }
            }
        }
    }
    
    
    @Test
    public void testSpecificWriteRead() throws Exception {
        serializator = new PPPCodeReadWriter(512, (short) 6, (long) Integer.MAX_VALUE);
        ByteBufferBits writeBufferBits = serializator.createNewBuffer(buffer);
        writeBufferBits.positionInBits(1);
        
        PPPCodeObject pppCodeObject = new PPPCodeObject(new int [] { 38920566, 38920574, 38920606, 38920611}, 
                new short [] { (short) 162, (short)15, (short)74, (short)139, (short)252});
        
        pppCodeObject.writeToBytes(writeBufferBits, serializator);

        ByteBufferBits readBufferBits = serializator.createNewBuffer(buffer);
        readBufferBits.positionInBits(1);
        PPPCodeObject readObject = serializator.readObject(readBufferBits, 5, null, null, 1, 0f);
        assertTrue(Arrays.equals(pppCodeObject.getLocators(), readObject.getLocators()));
        assertTrue(Arrays.equals(pppCodeObject.getPppForReading(), readObject.getPppForReading()));
    }    
    
    
    @Test
    public void testSimpleWriteRead() throws Exception {
        byte [] myBuffer = new byte[24];
        
        serializator = new PPPCodeReadWriter(512, (short) 6, (long) Integer.MAX_VALUE);
        ByteBufferBits writeBufferBits = serializator.createNewBuffer(myBuffer);
        writeBufferBits.positionInBits(3);
        
        int [] ids = new int [] { 38920566, 38920574, 38920606, 38920611};
        serializator.writeIDArray(writeBufferBits, ids);

        ByteBufferBits readBufferBits = serializator.createNewBuffer(myBuffer);
        readBufferBits.positionInBits(3);
        int[] readIDArray = serializator.readIDArray(readBufferBits);
        assertTrue(Arrays.equals(ids, readIDArray));
    }    
    
    @Test
    public void testNumberWriteRead() throws Exception {
        byte [] myBuffer = new byte[5];
        
        serializator = new PPPCodeReadWriter(512, (short) 6, (long) Integer.MAX_VALUE);
        ByteBufferBits writeBufferBits = serializator.createNewBuffer(myBuffer);
        writeBufferBits.positionInBits(2);
        
        int number = 8;
        writeBufferBits.put(number, 6);
        int number2 = 32;
        writeBufferBits.put(number2, 6);

        
        ByteBufferBits readBufferBits = serializator.createNewBuffer(myBuffer);
        readBufferBits.positionInBits(2);
        int readNumber = readBufferBits.get(6);
        int readNumber2 = readBufferBits.get(6);
        assertEquals(number, readNumber);
        assertEquals(number2, readNumber2);
    }  
    
    @Test
    public void testBitPrecision() {
        for (int i = 0; i <= 32; i++) {
            System.out.println("precision for " + i + ": " + PPPCodeReadWriter.getBitPrecisionForInt(i));
        }
        System.out.println("precision for " + Integer.MAX_VALUE + ": " + PPPCodeReadWriter.getBitPrecisionForInt(Integer.MAX_VALUE));        
    }
    /**
     * Test of writePPP method, of class PPPCodeSerializator.
     */
//    public void testWritePPP() throws Exception {
//        System.out.println("writePPP");        
//        short[] array = new short[] {(short) 3, (short) 2, (short) 1, (short) 0};
//        int tailLength = 4;
//        for (int i = 4; i <= 4; i *= 2) {
//            serializator = new PPPCodeSerializator(i);
//            byte [] result = serializator.writePPP(output, array, tailLength);
//            //int expResult = (int) Math.ceil((float)(4 * i) / (float)8);
//            //for (int j = 0; j < instance.byteSize(tailLength); j++) {
//            //assertEquals((byte) (0x00000032 & (3-j)), result[j]);
////                assertEquals((byte) (0x00000000), result[0]);
////                assertEquals((byte) (0x00000030), result[1]);
////                assertEquals((byte) (0x00000002), result[2]);
////                assertEquals((byte) (0x00000000), result[3]);
////                assertEquals((byte) (0x00000010), result[4]);
////                assertEquals((byte) (0x00000000), result[5]);
//            assertEquals((byte) (0x000000e4), result[0]);
//            assertEquals(-28, result[0]);
//            //}
//        }
//    }
}
