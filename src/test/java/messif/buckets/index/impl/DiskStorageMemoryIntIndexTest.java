/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.buckets.index.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import messif.objects.LocalAbstractObject;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class DiskStorageMemoryIntIndexTest {
    
    public DiskStorageMemoryIntIndexTest() {
    }
    
    @Before
    public void setUp() {
        
    }

    @Test
    public void testSomeMethod() {
        
        for (int sCSize = 1; sCSize < 5; sCSize++) {
            System.out.println("copy size: " + sCSize);
            for (int index = 0; index < 10; index++) {
                System.out.println("\tarray size: " + index);
                int indexesStep = index / (sCSize + 1);
                for (int i = 0; i < sCSize + 1; i++) {
                    System.out.println("\t\treading indexes [" + indexesStep * i + ", " + ((i != sCSize) ? (indexesStep * (i+1)) : Math.max(indexesStep * (i+1), index)) +")");
                }
            }
            
        }
    }
    
    @Test
    public void testIterator() {
        Random rand = new Random();
        for (int i = 0; i < 10; i++) {
            List<Iterator<Integer>> subIterators = new ArrayList<>(i);
            Integer [] [] results = new Integer [i][];
            int rightCounter = 0;
            for (int j = 0; j < i; j++) {                
                results[j] = new Integer [rand.nextInt(10)];
                for (int k = 0; k < results[j].length; k++) {
                    results[j][k] = rand.nextInt(100);
                }
                subIterators.add(Arrays.asList(results[j]).iterator());
                rightCounter += results[j].length;
            }
            Iterator<Integer> itOverIts = new ItOverIts(subIterators);
            int resultCounter = 0;
            while(itOverIts.hasNext()) {
                itOverIts.next();
                resultCounter ++;
            }
            
            System.out.println("counter: " + resultCounter);
            assertEquals(rightCounter, resultCounter);
        }        
    }
    
    protected static class ItOverIts implements Iterator<Integer> {

        private final List<Iterator<Integer>> subIterators;

        int currentId = -1;
        
        public ItOverIts(List<Iterator<Integer>> subIterators) {
            this.subIterators = subIterators;
            setNextNonemptyIt();
        }        
        
        private void setNextNonemptyIt() {
            if (subIterators.isEmpty()) {
                return;
            }
            if (++ currentId >= subIterators.size()) {
                currentId = 0;
            }
            while (! subIterators.get(currentId).hasNext()) {
                subIterators.remove(currentId);
                if (subIterators.isEmpty()) {
                    return;
                }
                if (currentId >= subIterators.size()) {
                    currentId = 0;
                }
            }
        }
        
        @Override
        public boolean hasNext() {
            return ! subIterators.isEmpty();
        }

        @Override
        public Integer next() {
            try {
                return (subIterators.get(currentId).next());
            } finally {
                setNextNonemptyIt();
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("This is read-only iterator."); //To change body of generated methods, choose Tools | Templates.
        }
    }    
    
}