# README #

The PPP-Codes library implements an indexing structure for large-scale similarity search as proposed in the following papers: 

D. Novak and P. Zezula, “Rank Aggregation of Candidate Sets for Efficient Similarity Search,” in Database and Expert Systems Applications: 25th International Conference, DEXA 2014, Munich, Germany, September 1-4, 2014. Proceedings, Part II, 2014, vol. 8645, pp. 42–58.

D. Novak and P. Zezula, “PPP-Codes for Large-Scale Similarity Searching,” Trans. Large-Scale Data- Knowledge-Centered Syst. XXIV, vol. 9510, pp. 61–87, 2016.

If you use the library for academic purposes, please reference these papers in your work.

### Who do I talk to? ###

* David Novak <david.novak(at)fi.muni.cz>

### Licence ###

PPP-Codes library is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. MESSIF library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.